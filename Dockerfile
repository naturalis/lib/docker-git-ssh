FROM docker:git

# hadolint ignore=DL3018
RUN apk del openssh* \
	&& apk update \
	&& apk --no-cache add openssh